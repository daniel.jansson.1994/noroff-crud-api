package no.noroff.webapiwithhibernate.controllers;

import no.noroff.webapiwithhibernate.models.Character;
import no.noroff.webapiwithhibernate.models.Franchise;
import no.noroff.webapiwithhibernate.models.Movie;
import no.noroff.webapiwithhibernate.repositories.FranchiseRepository;
import no.noroff.webapiwithhibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("api/v2/franchise")
public class FranchiseController {
    @Autowired
    private FranchiseRepository franchiseRepository;
    @Autowired
    private MovieRepository movieRepository;

    //Uses the standard URL to get a list of all franchises from the api.
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchiseRepository.findAll(), status);
    }

    //Uses the standard URL + "/newFranchise" to add a new franchise to the api.
    @PostMapping("/newFranchise")
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        return new ResponseEntity<>(franchiseRepository.save(franchise), HttpStatus.CREATED);
    }

    //Uses the standard URL + "/updateFranchise/id" to change the values to a current franchise.
    @PutMapping("/newValuesFranchise/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable int id, @RequestBody Franchise franchise) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if (id != franchise.getId()) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise, status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Franchise> updateCharacter(@PathVariable int id, @RequestBody int[] idChars){
        Franchise returnFranchise = franchiseRepository.getById(id);
        HttpStatus status;
        List<Movie> movies = new ArrayList<>();
        for (int i = 0; i < idChars.length; i++){
            Movie movie = movieRepository.getById(idChars[i]);
            movie.setFranchise(returnFranchise);
            movies.add(movie);
        }
        returnFranchise.setMovies(movies);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(franchiseRepository.save(returnFranchise), status);
    }


    //Uses the standard URL + "/deleteFranchiseId/id" to delete a given franchise.
    //For this to work, the movie connected to the franchise must lose its franchise connection.
    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteCharacter(@PathVariable int id) {
        Franchise franchise = franchiseRepository.getById(id);

        List<Integer> test = franchise.getMovies();
        for (int i = 0; i < test.size(); i++) {
            Movie movie = movieRepository.getById(test.get(i));
            movie.setFranchise(null);
        }
        franchiseRepository.delete(franchise);
        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", true);
        return response;
    }
}
