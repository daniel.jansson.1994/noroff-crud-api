package no.noroff.webapiwithhibernate.controllers;

import no.noroff.webapiwithhibernate.models.Character;
import no.noroff.webapiwithhibernate.models.Movie;
import no.noroff.webapiwithhibernate.repositories.CharacterRepository;
import no.noroff.webapiwithhibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("api/v2/movies")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;

    //Uses the standard URL to get a list of all movies from the api.
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies(){
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movieRepository.findAll(), status);
    }

    //Uses standard url + "/newMovie" to function, will allow the creation of a new movie.
    @PostMapping("/newMovie")
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        return new ResponseEntity<>(movieRepository.save(movie), HttpStatus.CREATED);
    }


    @PatchMapping("/newValuesCharacter/{id}")
    public ResponseEntity<Movie> updateCharacter(@PathVariable int id, @RequestBody int[] idChars){
        Movie returnMovie = movieRepository.getById(id);
        HttpStatus status;
        Set<Character> characters = new HashSet<>();
        for (int i = 0; i < idChars.length; i++){
            characters.add(characterRepository.getById(idChars[i]));
        }
        returnMovie.setCharacters(characters);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(movieRepository.save(returnMovie), status);
    }



    //Uses the standard URL + "/updateCharacter/id" to update the movie from the selected id.
    @PutMapping("/newMovie/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable int id, @RequestBody Movie movie){
        Movie returnMovie = new Movie();
        HttpStatus status;
        if (!(id == movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie, status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }

    //Uses the standard URL + "/deleteMovie/id" to set all movies with those characters to null then delete the movie.
    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteMovie(@PathVariable int id){
        Movie returnMovie = movieRepository.getById(id);

        List<Integer> test = returnMovie.getCharacters();
        for (int i = 0; i < test.size(); i++){
            Character character = characterRepository.getById(test.get(i));
            character.setMovies(null);
        }
        movieRepository.delete(returnMovie);

        Map<String, Boolean> response = new HashMap<>();
        response.put("Deleted", true);
        return response;
    }
}
