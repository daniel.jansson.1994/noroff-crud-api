package no.noroff.webapiwithhibernate.controllers;

import no.noroff.webapiwithhibernate.models.Character;
import no.noroff.webapiwithhibernate.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v2/characters")
public class CharacterController {
    @Autowired
    private CharacterRepository characterRepository;

    //Uses the standard URL to get all the characters stored in the api.
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters() {
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characterRepository.findAll(), status);
    }

    //Uses the standard URL + "/newCharacter" to add a new character to the api.
    @PostMapping("/newCharacter")
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        return new ResponseEntity<>(characterRepository.save(character), HttpStatus.CREATED);
    }

    //Uses the standard URL + "/updateCharacter/id" to change an existing characters attributes.
    @PutMapping("/newValues/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable int id, @RequestBody Character character) {
        Character returnCharacter = new Character();
        HttpStatus status;
        if (!(id == character.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter, status);
        }
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnCharacter, status);
    }

    //Uses the standard URL + "/deleteId/id" to delete a stored character.
    @DeleteMapping("/{id}")
    public ResponseEntity<Integer> deleteCharacter(@PathVariable int id) {

        characterRepository.delete(characterRepository.getById(id));
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

}
