package no.noroff.webapiwithhibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class will set up how the table for franchise will be used.
 * Relationships are also created so the data can connect with each other.
 */
@Entity
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @Column(length = 120)
    private String description;

    //Relationships
    //one franchise can contain many movies.
    @OneToMany(mappedBy = "franchises")
    private List<Movie> movies;

    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Franchise() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonGetter("movies")
    public List<Integer> getMovies() {
        if (movies != null) {
            return movies.stream().map(Movie::getId).collect(Collectors.toList());
        } else {
            return null;
        }

    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }


}
