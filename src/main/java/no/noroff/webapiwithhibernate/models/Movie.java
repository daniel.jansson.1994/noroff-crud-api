package no.noroff.webapiwithhibernate.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class will set up how the table for movie will be used.
 * Relationships are also created so the data can connect with each other.
 */
@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String title;
    @Column(nullable = false)
    private int releaseyear;
    @Column(length = 50, nullable = false)
    private String director;
    @Column(length = 100)
    private String poster;
    @Column(length = 100)
    private String trailer;

    //Relationships:
    //one movie contains many characters
    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    private Set<Character> characters = new HashSet<>();

    //Many movies can belong to one franchise.
    @ManyToOne
    private Franchise franchises;

    public Movie(String title, int releaseyear, String director, String poster, String trailer, Franchise franchises, Set<Character> characters) {
        this.title = title;
        this.releaseyear = releaseyear;
        this.director = director;
        this.poster = poster;
        this.trailer = trailer;
        this.franchises = franchises;
        this.characters = characters;
    }

    public Movie() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getReleaseyear() {
        return releaseyear;
    }

    public void setReleaseyear(int releaseyear) {
        this.releaseyear = releaseyear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }


    @JsonGetter("characters")
    public List<Integer> getCharacters() {
        return characters.stream().map(Character::getId).collect(Collectors.toList());
    }

    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }

    @JsonGetter("franchises")
    public Franchise getFranchise() {
        return franchises;
    }

    public void setFranchise(Franchise franchises) {
        this.franchises = franchises;
    }


}
