package no.noroff.webapiwithhibernate.models;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * This class will set up how the table for character will be used.
 * Relationships are also created so the data can connect with each other.
 */
@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String fullname;
    @Column(length = 50, nullable = false)
    private String alias;
    @Column(length = 10)
    private String gender;
    @Column(length = 200)
    private String photo;

    //Relationships
    //a character can play in multiple movies, one to many with movies
    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Movie> movies = new HashSet<>();

    public Character(String fullname, String alias, String gender, String photo) {
        this.fullname = fullname;
        this.alias = alias;
        this.gender = gender;
        this.photo = photo;
    }

    public Character() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }


    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }


}
