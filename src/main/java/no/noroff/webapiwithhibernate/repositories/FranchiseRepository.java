package no.noroff.webapiwithhibernate.repositories;

import no.noroff.webapiwithhibernate.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
}
