package no.noroff.webapiwithhibernate.dataseeder;

import no.noroff.webapiwithhibernate.models.Franchise;
import no.noroff.webapiwithhibernate.models.Movie;
import no.noroff.webapiwithhibernate.repositories.CharacterRepository;
import no.noroff.webapiwithhibernate.repositories.FranchiseRepository;
import no.noroff.webapiwithhibernate.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import no.noroff.webapiwithhibernate.models.Character;

import java.util.Set;
import java.util.stream.Collectors;


@Component
public class CharacterSeeder implements ApplicationRunner {
    @Autowired
    CharacterRepository characterRepository;
    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    MovieRepository movieRepository;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        loadCharacterData();
    }

    /**
     * This function will first check if any of the repositories has data connected to them, if not it will create new datasets.
     * The datasets will be created in a certain order to make sure movies can be populated with the correct franchise and characters.
     */
    private void loadCharacterData() {

        if (movieRepository.count() == 0 && characterRepository.count() == 0 && franchiseRepository.count() == 0) {
            final Franchise franchise1 = franchiseRepository.save(new Franchise("MCU", "Disney 4 President"));
            final Franchise franchise2 = franchiseRepository.save(new Franchise("DCU", "Marvel Who?"));
            final Franchise franchise3 = franchiseRepository.save(new Franchise("Disney", "We use your kids for cashgrabs."));
            final Franchise franchise4 = franchiseRepository.save(new Franchise("The Conjuring Universe", "We make you spooked"));
            final Franchise franchise5 = franchiseRepository.save(new Franchise("Haha Universe", "We make you pepepoint"));
            final Character char1 = characterRepository.save(new Character("Tom Holland", "Spodermen", "Male", "https://upload.wikimedia.org/wikipedia/commons/f/fd/Tom_Holland_MTV_2018_%2802%29.jpg"));
            final Character char2 = characterRepository.save(new Character("Benedict Cumberbatch", "Dr.Strange", "Male", "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/BCumberbatch_Comic-Con_2019.jpg/375px-BCumberbatch_Comic-Con_2019.jpg"));
            final Character char3 = characterRepository.save(new Character("Robert Downey Jr.", "Iron Man", "Male", "https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg/375px-Robert_Downey_Jr_2014_Comic_Con_%28cropped%29.jpg"));
            final Character char4 = characterRepository.save(new Character("Scarlett Johansson", "Black Widow", "Female", "https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Scarlett_Johansson_by_Gage_Skidmore_2_%28cropped%29.jpg/375px-Scarlett_Johansson_by_Gage_Skidmore_2_%28cropped%29.jpg"));
            final Character char5 = characterRepository.save(new Character("Marisa Tomei", "Aunt May", "Female", "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Marisa_Tomei_TIFF_2012_%28cropped%29.jpg/375px-Marisa_Tomei_TIFF_2012_%28cropped%29.jpg"));
            final Character char6 = characterRepository.save(new Character("Zendaya", "MJ", "Female", "https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Zendaya_-_2019_by_Glenn_Francis.jpg/375px-Zendaya_-_2019_by_Glenn_Francis.jpg"));
            final Character char7 = characterRepository.save(new Character("Ian McKellen", "Gandalf", "Male", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQGgAkVS_q--4v139BPH00uOsz1eXSWIJYahmOUOSb1gXwETbNe"));
            final Character char8 = characterRepository.save(new Character("Andy Serkis", "Gollum", "Male", "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Andy_Serkis_Comic-Con_2011.jpg/375px-Andy_Serkis_Comic-Con_2011.jpg"));
            final Character char9 = characterRepository.save(new Character("Ian Holm", "Bilbo Baggins", "Male", "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Ian_Holm.jpg/375px-Ian_Holm.jpg"));

            final Movie movie1 = movieRepository.save(new Movie("The return of the Avengers", 2025, "Nick & Dewald", null, null, franchiseRepository.getById(1), Set.of(char1, char2)));
            final Movie movie2 = movieRepository.save(new Movie("Avenging the underworld", 2023, "Brad Pitt", null, null, franchiseRepository.getById(2), Set.of(char1, char4)));
            final Movie movie3 = movieRepository.save(new Movie("The return of the fellowship of the rings", 2030, "Dave & Dan", null, null, franchiseRepository.getById(4), Set.of(char1, char5)));
            final Movie movie4 = movieRepository.save(new Movie("Who killed Dr.Strange?", 2030, "Benedict Cumberbatch", null, null, franchiseRepository.getById(5), Set.of(char1, char2)));
            final Movie movie5 = movieRepository.save(new Movie("Spider-man: Homewrecking", 2050, "Tom Holland", null, null, franchiseRepository.getById(1), Set.of(char1, char6, char9)));

        }

    }

}
