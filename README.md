# Noroff assignment: Access and expose a database.

## Authors:
- Daniel Jansson

## Technologies/Dependencies:
- Java 17
- Spring boot
- Hibernate
- PostgreSQL

## Link to view frontend through heroku:

- [Swaggerpage](https://hibernate-api-noroff.herokuapp.com/swagger-ui/index.html)

## Link to view backend through heroku:

- [All characters](https://hibernate-api-noroff.herokuapp.com/api/v2/characters)
- [All movies](https://hibernate-api-noroff.herokuapp.com/api/v2/movies)
- [All franchises](https://hibernate-api-noroff.herokuapp.com/api/v2/franchise)